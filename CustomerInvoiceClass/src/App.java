import com.devcamp.task53b_30.Customer;
import com.devcamp.task53b_30.Invoice;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Anh", 10);
        Customer customer2 = new Customer(2, "Anh Luong", 20);
        System.out.println("customer1: " + customer1);
        System.out.println("customer2: " + customer2);

        Invoice invoice1 = new Invoice(1, customer1, 100000);
        Invoice invoice2 = new Invoice(2, customer2, 200000);
        System.out.println("invoice1: " + invoice1 + " ,amount after discount: " + invoice1.getAmountAfterDiscount());
        System.out.println("invoice2: " + invoice2 + " ,amount after discount: " + invoice2.getAmountAfterDiscount());
    }
}
